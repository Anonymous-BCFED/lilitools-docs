import { defineConfig } from "vitepress";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "LiliTools Documentation",
  description: "Save Editors and Tools for Lilith's Throne",
  head: [["link", { rel: "icon", href: "/lilitool-smol.svg" }]],
  outDir: "../public",
  base: "/lilitools-docs/",
  appearance: "force-dark",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    logo: { src: "/lilitool-smol.svg", width: 24, height: 24 },
    nav: [
      { text: "Home", link: "/" },
      { text: "Guide", link: "/guide/" },
      { text: "Commands", link: "/commands/" },
      { text: "Reference", link: "/ref/" },
      { text: "Development", link: "/dev/" },
    ],

    sidebar: [
      {
        text: "Guide",
        items: [
          { text: "Introduction", link: "/guide/" },
          { text: "Installing", link: "/guide/installing" },
          { text: "Running lilirun", link: "/guide/lilirun-tutorial" },
          { text: "lilitool Tutorial", link: "/guide/lilitool-tutorial" },
        ],
      },
      {
        text: "Commands",
        items: [
          { text: "Introduction", link: "/commands/" },
          { text: "lilirun", link: "/commands/lilirun/" },
          {
            text: "slavesheet",
            link: "/commands/slavesheet/",
            items: [
              { text: "import", link: "/commands/slavesheet/import" },
              { text: "serve", link: "/commands/slavesheet/serve" },
              { text: "slave", link: "/commands/slavesheet/slave" },
            ],
          },
          {
            text: "lilitool",
            link: "/commands/lilitool/",
            items: [
              { text: "backup", link: "/commands/lilitool/backup" },
              {
                text: "enchant",
                link: "/commands/lilitool/enchant/",
                items: [
                  {
                    text: "clothing",
                    link: "/commands/lilitool/enchant/clothing",
                  },
                ],
              },
              {
                text: "heal",
                link: "/commands/lilitool/heal",
              },
              {
                text: "inventory",
                link: "/commands/lilitool/inventory/",
                items: [
                  {
                    text: "remove",
                    link: "/commands/lilitool/inventory/remove",
                  },
                ],
              },
              {
                text: "player",
                link: "/commands/lilitool/player/",
                items: [
                  {
                    text: "body",
                    link: "/commands/lilitool/player/body",
                  },
                  {
                    text: "fetish",
                    link: "/commands/lilitool/player/fetish",
                  },
                  {
                    text: "perk",
                    link: "/commands/lilitool/player/perk",
                  },
                  {
                    text: "show",
                    link: "/commands/lilitool/player/show",
                  },
                ],
              },
              {
                text: "unseal",
                link: "/commands/lilitool/unseal",
              },
            ],
          },
        ],
      },
      {
        text: "Reference",
        items: [
          { text: "Index", link: "/ref/" },
          {
            text: "Lilith's Throne",
            link: "/ref/lt/",
            items: [
              { text: "Character ID", link: "/ref/lt/character-id" },
              {
                text: "Known Effects",
                link: "/ref/lt/known_effects/",
                collapsed: true,
                items: [
                  {
                    text: "ADDICTION_REMOVAL_REFINEMENT",
                    link: "/ref/lt/known_effects/ADDICTION_REMOVAL_REFINEMENT.md",
                  },
                  {
                    text: "ATTRIBUTE_ARCANE",
                    link: "/ref/lt/known_effects/ATTRIBUTE_ARCANE.md",
                  },
                  {
                    text: "ATTRIBUTE_CORRUPTION",
                    link: "/ref/lt/known_effects/ATTRIBUTE_CORRUPTION.md",
                  },
                  {
                    text: "ATTRIBUTE_PHYSIQUE",
                    link: "/ref/lt/known_effects/ATTRIBUTE_PHYSIQUE.md",
                  },
                  {
                    text: "ATTRIBUTE_SEXUAL",
                    link: "/ref/lt/known_effects/ATTRIBUTE_SEXUAL.md",
                  },
                  {
                    text: "CLOTHING",
                    link: "/ref/lt/known_effects/CLOTHING.md",
                  },
                  {
                    text: "EGGPLANT_POTION",
                    link: "/ref/lt/known_effects/EGGPLANT_POTION.md",
                  },
                  {
                    text: "FETISH_ENHANCEMENT",
                    link: "/ref/lt/known_effects/FETISH_ENHANCEMENT.md",
                  },
                  {
                    text: "ORIENTATION_CHANGE",
                    link: "/ref/lt/known_effects/ORIENTATION_CHANGE.md",
                  },
                  {
                    text: "RACE_ALLIGATOR_MORPH",
                    link: "/ref/lt/known_effects/RACE_ALLIGATOR_MORPH.md",
                  },
                  {
                    text: "RACE_ANGEL",
                    link: "/ref/lt/known_effects/RACE_ANGEL.md",
                  },
                  {
                    text: "RACE_BAT_MORPH",
                    link: "/ref/lt/known_effects/RACE_BAT_MORPH.md",
                  },
                  {
                    text: "RACE_CAT_MORPH",
                    link: "/ref/lt/known_effects/RACE_CAT_MORPH.md",
                  },
                  {
                    text: "RACE_COW_MORPH",
                    link: "/ref/lt/known_effects/RACE_COW_MORPH.md",
                  },
                  {
                    text: "RACE_DEMON",
                    link: "/ref/lt/known_effects/RACE_DEMON.md",
                  },
                  {
                    text: "RACE_DOG_MORPH",
                    link: "/ref/lt/known_effects/RACE_DOG_MORPH.md",
                  },
                  {
                    text: "RACE_DOLL",
                    link: "/ref/lt/known_effects/RACE_DOLL.md",
                  },
                  {
                    text: "RACE_ELEMENTAL",
                    link: "/ref/lt/known_effects/RACE_ELEMENTAL.md",
                  },
                  {
                    text: "RACE_FOX_MORPH",
                    link: "/ref/lt/known_effects/RACE_FOX_MORPH.md",
                  },
                  {
                    text: "RACE_HARPY",
                    link: "/ref/lt/known_effects/RACE_HARPY.md",
                  },
                  {
                    text: "RACE_HORSE_MORPH",
                    link: "/ref/lt/known_effects/RACE_HORSE_MORPH.md",
                  },
                  {
                    text: "RACE_HUMAN",
                    link: "/ref/lt/known_effects/RACE_HUMAN.md",
                  },
                  {
                    text: "RACE_NONE",
                    link: "/ref/lt/known_effects/RACE_NONE.md",
                  },
                  {
                    text: "RACE_NoStepOnSnek_capybara",
                    link: "/ref/lt/known_effects/RACE_NoStepOnSnek_capybara.md",
                  },
                  {
                    text: "RACE_NoStepOnSnek_octopus",
                    link: "/ref/lt/known_effects/RACE_NoStepOnSnek_octopus.md",
                  },
                  {
                    text: "RACE_NoStepOnSnek_snake",
                    link: "/ref/lt/known_effects/RACE_NoStepOnSnek_snake.md",
                  },
                  {
                    text: "RACE_RABBIT_MORPH",
                    link: "/ref/lt/known_effects/RACE_RABBIT_MORPH.md",
                  },
                  {
                    text: "RACE_RAT_MORPH",
                    link: "/ref/lt/known_effects/RACE_RAT_MORPH.md",
                  },
                  {
                    text: "RACE_REINDEER_MORPH",
                    link: "/ref/lt/known_effects/RACE_REINDEER_MORPH.md",
                  },
                  {
                    text: "RACE_SLIME",
                    link: "/ref/lt/known_effects/RACE_SLIME.md",
                  },
                  {
                    text: "RACE_SQUIRREL_MORPH",
                    link: "/ref/lt/known_effects/RACE_SQUIRREL_MORPH.md",
                  },
                  {
                    text: "RACE_WOLF_MORPH",
                    link: "/ref/lt/known_effects/RACE_WOLF_MORPH.md",
                  },
                  {
                    text: "RACE_charisma_spider",
                    link: "/ref/lt/known_effects/RACE_charisma_spider.md",
                  },
                  {
                    text: "RACE_dsg_bear",
                    link: "/ref/lt/known_effects/RACE_dsg_bear.md",
                  },
                  {
                    text: "RACE_dsg_dragon",
                    link: "/ref/lt/known_effects/RACE_dsg_dragon.md",
                  },
                  {
                    text: "RACE_dsg_ferret",
                    link: "/ref/lt/known_effects/RACE_dsg_ferret.md",
                  },
                  {
                    text: "RACE_dsg_gryphon",
                    link: "/ref/lt/known_effects/RACE_dsg_gryphon.md",
                  },
                  {
                    text: "RACE_dsg_otter",
                    link: "/ref/lt/known_effects/RACE_dsg_otter.md",
                  },
                  {
                    text: "RACE_dsg_raccoon",
                    link: "/ref/lt/known_effects/RACE_dsg_raccoon.md",
                  },
                  {
                    text: "RACE_dsg_shark",
                    link: "/ref/lt/known_effects/RACE_dsg_shark.md",
                  },
                  {
                    text: "RACE_innoxia_badger",
                    link: "/ref/lt/known_effects/RACE_innoxia_badger.md",
                  },
                  {
                    text: "RACE_innoxia_deer",
                    link: "/ref/lt/known_effects/RACE_innoxia_deer.md",
                  },
                  {
                    text: "RACE_innoxia_goat",
                    link: "/ref/lt/known_effects/RACE_innoxia_goat.md",
                  },
                  {
                    text: "RACE_innoxia_hyena",
                    link: "/ref/lt/known_effects/RACE_innoxia_hyena.md",
                  },
                  {
                    text: "RACE_innoxia_mouse",
                    link: "/ref/lt/known_effects/RACE_innoxia_mouse.md",
                  },
                  {
                    text: "RACE_innoxia_panther",
                    link: "/ref/lt/known_effects/RACE_innoxia_panther.md",
                  },
                  {
                    text: "RACE_innoxia_pig",
                    link: "/ref/lt/known_effects/RACE_innoxia_pig.md",
                  },
                  {
                    text: "RACE_innoxia_raptor",
                    link: "/ref/lt/known_effects/RACE_innoxia_raptor.md",
                  },
                  {
                    text: "RACE_innoxia_sheep",
                    link: "/ref/lt/known_effects/RACE_innoxia_sheep.md",
                  },
                  {
                    text: "RACE_mintychip_salamander",
                    link: "/ref/lt/known_effects/RACE_mintychip_salamander.md",
                  },
                  {
                    text: "TATTOO",
                    link: "/ref/lt/known_effects/TATTOO.md",
                  },
                  {
                    text: "WEAPON",
                    link: "/ref/lt/known_effects/WEAPON.md",
                  },
                ],
              },
            ],
          },
          {
            text: "LiliTools",
            link: "/ref/lilitools/",
            items: [{ text: "SID", link: "/ref/lilitools/sid" }],
          },
        ],
      },
      {
        text: "Development",
        items: [
          { text: "Introduction", link: "/dev/" },
          { text: "Adding Stuff", link: "/dev/adding-features.md" },
          { text: "Writing Docs", link: "/dev/writing-docs.md" },
          { text: "Important Locations", link: "/dev/important_locations.md" },
          { text: "Update LT", link: "/dev/update_lt.md" },
        ],
      },
    ],

    socialLinks: [
      {
        icon: {
          svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 380 380"><defs><style>.cls-1{fill:#e24329;}.cls-2{fill:#fc6d26;}.cls-3{fill:#fca326;}</style></defs><g id="LOGO"><path class="cls-1" d="M282.83,170.73l-.27-.69-26.14-68.22a6.81,6.81,0,0,0-2.69-3.24,7,7,0,0,0-8,.43,7,7,0,0,0-2.32,3.52l-17.65,54H154.29l-17.65-54A6.86,6.86,0,0,0,134.32,99a7,7,0,0,0-8-.43,6.87,6.87,0,0,0-2.69,3.24L97.44,170l-.26.69a48.54,48.54,0,0,0,16.1,56.1l.09.07.24.17,39.82,29.82,19.7,14.91,12,9.06a8.07,8.07,0,0,0,9.76,0l12-9.06,19.7-14.91,40.06-30,.1-.08A48.56,48.56,0,0,0,282.83,170.73Z"/><path class="cls-2" d="M282.83,170.73l-.27-.69a88.3,88.3,0,0,0-35.15,15.8L190,229.25c19.55,14.79,36.57,27.64,36.57,27.64l40.06-30,.1-.08A48.56,48.56,0,0,0,282.83,170.73Z"/><path class="cls-3" d="M153.43,256.89l19.7,14.91,12,9.06a8.07,8.07,0,0,0,9.76,0l12-9.06,19.7-14.91S209.55,244,190,229.25C170.45,244,153.43,256.89,153.43,256.89Z"/><path class="cls-2" d="M132.58,185.84A88.19,88.19,0,0,0,97.44,170l-.26.69a48.54,48.54,0,0,0,16.1,56.1l.09.07.24.17,39.82,29.82s17-12.85,36.57-27.64Z"/></g></svg>',
        },
        link: "https://gitlab.com/Anonymous-BCFED/LiliTools",
      },
    ],
  },
});
