// https://vitepress.dev/guide/custom-theme
import { h } from 'vue'
import Key from './components/Key.vue'
import WinKey from './components/WinKey.vue'
import EnterKey from "./components/EnterKey.vue";
import type { Theme } from 'vitepress'
import DefaultTheme from 'vitepress/theme'
import './style.css'

export default {
  extends: DefaultTheme,
  Layout: () => {
    return h(DefaultTheme.Layout, null, {
      // https://vitepress.dev/guide/extending-default-theme#layout-slots
    })
  },
  enhanceApp({ app, router, siteData }) {
    app
      .component("Key", Key)
      .component("WinKey", WinKey)
      .component("EnterKey", EnterKey);
  }
} satisfies Theme
