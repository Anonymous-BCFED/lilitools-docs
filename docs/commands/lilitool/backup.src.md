# `lilitool backup` Subcommand

`lilitool backup` creates and updates a git repository with a backup of your saves.

::: note
Older versions of lilitool required [git](https://git-scm.org) and [git-lfs](https://git-lfs.com/) to be installed and available on PATH.

Newer versions make use of dulwich, which is installed as part of lilitools. You can still use git by specifying `--git-lib=git-cli` with `backup`.
:::

## Usage

::: info
The first time you run this command requires you to use `--data-dir` to tell lilitool where to look for your saves.
:::

<@cmd{"args":["lilitool","backup","--help"]}@>

## Examples

<@cmd{"args":["lilitool","backup","-m","Message for the backup","--data-dir=data"],"replacements":{"\\[master \\(root\\-commit\\) ([a-f0-9]+)\\]":"[master (root-commit) 0000000]","\\[master ([a-f0-9]+)\\] Message for the backup":"[master 0000000] Message for the backup"}}@>
