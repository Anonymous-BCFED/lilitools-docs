# `lilitool enchant clothing` Subcommand

The `lilitool enchant clothing` subcommand provides you with a powerful toolkit for creating, removing and manipulating enchantments on clothing.

::: info
This command is very complex, but should be easy to learn with patience.
:::

## Usage

<@cmd{"args":["lilitool","enchant","clothing","--help"]}@>

## Arguments