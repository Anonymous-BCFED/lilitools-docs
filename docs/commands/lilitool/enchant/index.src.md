# `lilitool enchant` Subcommand

The `lilitool enchant` subcommands give you a powerful toolkit for producing enchantments and potions.

::: info
These commands are very complex, but should be easy to learn, with patience.
:::

## Usage

<@cmd{"args":["lilitool","enchant","--help"]}@>

## Subcommands

* [clothing](clothing)