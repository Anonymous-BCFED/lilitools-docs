# `lilitool heal` Subcommand

`lilitool heal` can diagnose and heal the player character or NPCs in the game.

::: warning
This command isn't exact, due to the slight differences in implementation of math in Java and Python.
:::

## Usage

<@cmd{"args":["lilitool","heal","--help"]}@>

## Examples

### Heal all Damage, Aura, and Lust

<@cmd{"args":["lilitool","heal","--all","--player","data/saves/female.xml"]}@>