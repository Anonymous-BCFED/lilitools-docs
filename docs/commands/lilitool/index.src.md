# `lilitool` Command Reference

The following is a reference manual for all the commands available for `lilitool`.

## Usage

<@cmd{"args":["lilitool","--help"]}@>

## Global Options

::: info
The following command line options are available for **all** commands.
:::

| Long Form             | Short Form | Type     | Description                                                                   |
| --------------------- | ---------- | -------- | ----------------------------------------------------------------------------- |
| `--debug`             |            |          | Enables debug output                                                          |
| `--log-level=<LEVEL>` |            | Choice   | Set logging level (choice of `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`) |
| `--version`           |            | Function | Displays the version of lilitool, then exits.                                 |
| `--help`              | `-h`       | Function | Displays the help page for the current subcommand, then exits.                |

## Subcommands

* [backup](backup)
* [enchant](enchant/)
* [heal](heal)
* [inv](inventory/)
* [inventory](inventory/)
<!-- * [npc](npc) -->
<!-- * [npcs](npcs) -->
* [pc](player/)
* [player](player/)
<!-- * [pregnancy](pregnancy) -->
<!-- * [room](room) -->
<!-- * [save](save) -->
* [unseal](unseal)
