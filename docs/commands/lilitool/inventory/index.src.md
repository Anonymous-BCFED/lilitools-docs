# `lilitool inventory` Subcommand

The `lilitool inventory` subcommands allow you to modify inventories.

## Usage

<@cmd{"args":["lilitool","inventory","--help"]}@>

## Common Arguments

### Inventory Search

Several commands use a search algorithm to quickly find and act on inventory items. Below are the command line arguments to tell the search what to look for.

| Short Form | Long Form             | Type    | Notes                                                                                 |
| ---------- | --------------------- | ------- | ------------------------------------------------------------------------------------- |
| `-i`       | `--id`                | string  | The [item type ID](/ref/lt/types/). Can be specified multiple times for an OR search. |
| `-n`       | `--name`              | string  | The name of the item. Can be specified multiple times for an OR search.               |
|            | `--sealed`            | boolean | Whether the item is sealed or not.                                                    |
|            | `--enchantment-known` | boolean | Whether the item's enchantments are known or not.                                     |

## Subcommands

- [remove](remove)
