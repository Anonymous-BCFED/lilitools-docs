# `lilitool inventory remove` Subcommand

The `lilitool inventory remove` subcommand allows you to remove items that match an [inventory search](/commands/lilitool/inventory/#inventory-search).

## Usage

<@cmd{"args":["lilitool","inventory","remove","--help"]}@> 

## Examples

### Removing All Items By Name

Let's say you have something called Ring of Ugly in your inventory, and you wish to remove all of them.

<@cmd{"args":["lilitool","inv","rm","--player","--name=Ring of Ugly","--actually-remove","data/saves/female.xml"]}@>
