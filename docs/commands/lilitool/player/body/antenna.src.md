<Breadcrumbs />

# `lilitool player body antenna` Subcommand

`lilitool player body antenna` allows you to modify variables of any antenna present on the player character.

## Usage

<@cmd{"args":["lilitool","player","body","antenna","--help"]}@>