<Breadcrumbs />

# `lilitool player body core` Subcommand

`lilitool player body core` allows you to modify core body variables of the player character in the save.

## Usage

<!-- AUTOGENERATED: <@cmd{"args":["lilitool","player","body","core","--help"]}@> -->
```shell
lilitool player body core --help
```
```
usage: lilitool player body core [-h] [--set-birth-day SET_BIRTH_DAY]
                                 [--set-birth-month SET_BIRTH_MONTH]
                                 [--set-birth-year SET_BIRTH_YEAR]
                                 [--set-name-all SET_NAME_ALL]
                                 [--set-name-and SET_NAME_AND]
                                 [--set-name-fem SET_NAME_FEM]
                                 [--set-name-mas SET_NAME_MAS]
                                 [--set-elemental-id SET_ELEMENTAL_ID]
                                 [--unset-elemental-id]
                                 [--set-elemental-summoned SET_ELEMENTAL_SUMMONED]
                                 [--add-desired-job [ADD_DESIRED_JOB ...]]
                                 [--rm-desired-job [RM_DESIRED_JOB ...]]
                                 [--clear-desired-jobs]
                                 [--set-age-appearance-difference SET_AGE_APPEARANCE_DIFFERENCE]
                                 [--set-captive SET_CAPTIVE]
                                 [--set-combat-behaviour SET_COMBAT_BEHAVIOUR]
                                 [--set-description SET_DESCRIPTION]
                                 [--set-first-name-terms SET_FIRST_NAME_TERMS]
                                 [--set-generic-name SET_GENERIC_NAME]
                                 [--unset-generic-name]
                                 [--set-history SET_HISTORY]
                                 [--set-last-time-had-sex SET_LAST_TIME_HAD_SEX]
                                 [--set-last-time-orgasmed SET_LAST_TIME_ORGASMED]
                                 [--set-level SET_LEVEL]
                                 [--set-race-concealed SET_RACE_CONCEALED]
                                 [--set-speech-colour SET_SPEECH_COLOUR]
                                 [--unset-speech-colour]
                                 [--set-surname SET_SURNAME] [--dry-run]
                                 path

positional arguments:
  path                  Path to the save XML

options:
  -h, --help            show this help message and exit
  --set-age-appearance-difference SET_AGE_APPEARANCE_DIFFERENCE
  --set-captive SET_CAPTIVE
  --set-combat-behaviour SET_COMBAT_BEHAVIOUR
  --set-description SET_DESCRIPTION
  --set-first-name-terms SET_FIRST_NAME_TERMS
  --set-generic-name SET_GENERIC_NAME
                        Set the generic name of an NPC
  --unset-generic-name  Set the generic name of an NPC to null (None)
  --set-history SET_HISTORY
  --set-last-time-had-sex SET_LAST_TIME_HAD_SEX
  --set-last-time-orgasmed SET_LAST_TIME_ORGASMED
  --set-level SET_LEVEL
  --set-race-concealed SET_RACE_CONCEALED
  --set-speech-colour SET_SPEECH_COLOUR
  --unset-speech-colour
  --set-surname SET_SURNAME
  --dry-run             Don't actually do anything.

Birthday:
  --set-birth-day SET_BIRTH_DAY
  --set-birth-month SET_BIRTH_MONTH
  --set-birth-year SET_BIRTH_YEAR

First Name:
  --set-name-all SET_NAME_ALL
                        Set all three names
  --set-name-and SET_NAME_AND, --set-name-androgynous SET_NAME_AND
                        Set androgynous first name
  --set-name-fem SET_NAME_FEM, --set-name-feminine SET_NAME_FEM
                        Set feminine first name
  --set-name-mas SET_NAME_MAS, --set-name-masculine SET_NAME_MAS
                        Set masculine first name

Elemental:
  --set-elemental-id SET_ELEMENTAL_ID
  --unset-elemental-id
  --set-elemental-summoned SET_ELEMENTAL_SUMMONED

Desired Jobs:
  --add-desired-job [ADD_DESIRED_JOB ...]
  --rm-desired-job [RM_DESIRED_JOB ...]
  --clear-desired-jobs
```
<!-- END AUTOGENERATED-->