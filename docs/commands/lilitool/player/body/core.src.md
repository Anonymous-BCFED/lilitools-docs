<Breadcrumbs />

# `lilitool player body core` Subcommand

`lilitool player body core` allows you to modify core body variables of the player character in the save.

## Usage

<@cmd{"args":["lilitool","player","body","core","--help"]}@>