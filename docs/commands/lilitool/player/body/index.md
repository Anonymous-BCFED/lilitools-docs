<Breadcrumbs />

# `lilitool player body` Subcommand

`lilitool player body` provides a set of tools for interacting with the player's body in a save.

::: warning

This subcommand is still receiving updates.  Expect changes!

:::

## Usage

<!-- AUTOGENERATED: <@cmd{"args":["lilitool","player","body","--help"]}@> -->
```shell
lilitool player body --help
```
```
usage: lilitool player body [-h] {core,antenna,antennae} ...

positional arguments:
  {core,antenna,antennae}

options:
  -h, --help            show this help message and exit
```
<!-- END AUTOGENERATED-->

## Subcommands

* [core](core) - Core body information
* [antenna](antenna) (antennae) - Editors for antennae