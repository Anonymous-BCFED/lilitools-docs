<Breadcrumbs />

# `lilitool player body` Subcommand

`lilitool player body` provides a set of tools for interacting with the player's body in a save.

::: warning

This subcommand is still receiving updates.  Expect changes!

:::

## Usage

<@cmd{"args":["lilitool","player","body","--help"]}@>

## Subcommands

* [core](core) - Core body information
* [antenna](antenna) (antennae) - Editors for antennae