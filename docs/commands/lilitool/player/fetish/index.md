<Breadcrumbs />

# `lilitool player fetish` Subcommand

`lilitool player fetish` provides a set of tools for interacting with the player's fetishes and desires in a save.

## Usage

<!-- AUTOGENERATED: <@cmd{"args":["lilitool","player","fetish","--help"]}@> -->
```shell
lilitool player fetish --help
```
```
usage: lilitool player fetishes [-h] [--all-known-fetishes]
                                [--all-player-fetishes] [--list]
                                [--list-by-desire] [--dry-run] [--reset]
                                [--set-desire SET_DESIRE] [--set-xp SET_XP]
                                [--set-is-fetish SET_IS_FETISH]
                                path [fetish_ids ...]

positional arguments:
  path                  Path to the save XML
  fetish_ids

options:
  -h, --help            show this help message and exit
  --all-known-fetishes  All fetishes known to the editor
  --all-player-fetishes
                        All fetishes known to the PC, liked or disliked
  --list
  --list-by-desire
  --dry-run             Do not save after modifying.
  --reset               Reset all desires to 0 xp, non-fetish, and NEUTRAL
                        desire
  --set-desire SET_DESIRE
  --set-xp SET_XP
  --set-is-fetish SET_IS_FETISH
```
<!-- END AUTOGENERATED-->

<!-- ## Subcommands

* [body](body/) - Body modification
* [fetish](fetish/) (fetishes) - Fetish editors
* [perk](perk/) (perks) - Perk editors
* [show](show) - Display character -->