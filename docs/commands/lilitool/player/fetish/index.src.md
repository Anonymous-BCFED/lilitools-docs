<Breadcrumbs />

# `lilitool player fetish` Subcommand

`lilitool player fetish` provides a set of tools for interacting with the player's fetishes and desires in a save.

## Usage

<@cmd{"args":["lilitool","player","fetish","--help"]}@>

<!-- ## Subcommands

* [body](body/) - Body modification
* [fetish](fetish/) (fetishes) - Fetish editors
* [perk](perk/) (perks) - Perk editors
* [show](show) - Display character -->