<Breadcrumbs />

# `lilitool player` Subcommand

`lilitool player` provides a set of tools for interacting with the player in a save.


## Usage

::: info
The first time you run this command requires you to use `--data-dir` to tell lilitool where to look for your saves.
:::

<@cmd{"args":["lilitool","player","--help"]}@>

## Subcommands

* [body](body/) - Body modification
* [fetish](fetish/) (fetishes) - Fetish editors
* [perk](perk/) (perks) - Perk editors
* [show](show) - Display character