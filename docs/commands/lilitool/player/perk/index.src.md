<Breadcrumbs />

# `lilitool player perk` Subcommand

`lilitool player perk` provides a set of tools for interacting with the player's perks in a save.

## Usage

<@cmd{"args":["lilitool","player","perk","--help"]}@>

<!-- ## Subcommands

* [body](body/) - Body modification
* [fetish](fetish/) (fetishes) - Fetish editors
* [perk](perk/) (perks) - Perk editors
* [show](show) - Display character -->