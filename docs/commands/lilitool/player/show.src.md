<Breadcrumbs />

# `lilitool player show` Subcommand

The `lilitool player show` subcommand  simply displays the internal state of the player as stored in the save.

## Usage

<@cmd{"args":["lilitool","player","show","--help"]}@>

## Examples

### Display a female player character
<@cmd{"args":["lilitool","player","show","data/saves/female.xml"]}@>

### Display a male player character
<@cmd{"args":["lilitool","player","show","data/saves/male.xml"]}@>