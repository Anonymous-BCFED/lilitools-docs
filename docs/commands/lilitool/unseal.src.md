# `lilitool unseal` Subcommand

`lilitool unseal` can remove seals from the player's worn clothing.

::: warning
Use of this command may result in unforseen consequences
:::

## Usage

<@cmd{"args":["lilitool","unseal","--help"]}@>


## Examples

### Unseal all clothing
<@cmd{"args":["lilitool","unseal","data/saves/female.xml","--player","--all"]}@>