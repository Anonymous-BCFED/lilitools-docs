<Breadcrumbs />

# slavesheet

`slavesheet` is a tool that presents your slaves in a simplified spreadsheet form, and provides a few other useful tools, such as a slave job gantt chart. It provides several sorting and planning options.

<@cmd{"args":["slavesheet","--help"]}@>

## A Note

::: warning

`slavesheet` was made with myself and my own requirements in mind. It may not fit your needs. While it tries to provide as much information as possible, much of it may not be applicable to you or just completely useless. Your milage may vary.

:::

## Subcommands

- [import](./import.md)
- [serve](./serve.md)
- [slave](./slave/index.md)
