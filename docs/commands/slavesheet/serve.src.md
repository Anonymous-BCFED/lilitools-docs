<Breadcrumbs />

# `slavesheet serve` Command

`slavesheet serve` will boot up a tiny webserver (by default bound to 127.0.0.1 so nobody else can mess with it) to show you your slaves and some other controls and tools.

::: danger

Never set `--bind` to anything other than `localhost` or `127.0.0.1`, or you could open yourself up to attack.

:::

## Usage

<@cmd{"args":["slavesheet","serve","--help"]}@>

## UI

### Slave View

![Slavesheet Slave View](../../public/screenshots/slavesheet-view-slaves.png)

The Slave View provides a comprehensive table of all of your slaves in a sortable, searchable way.

Double-clicking a slave will bring up an editor for changing notes and planning information.

Clicking a slave SID or CID will copy it to your clipboard.

### Training Rooms View

![Slavesheet Training Rooms View](../../public/screenshots/slavesheet-view-training-rooms.png)

The Training Rooms View lists all known "training rooms" (rooms where slaves are trained by room upgrades). These are listed to help with lodging planning.

The two currently-recognized training room types are:

<@data table_main_room_regex {} @>

There are also other rooms possible, but they will not be added to the list automatically.

<@data table_misc_room_regex {} @>

::: note

\* = These rooms are currently not used in slavesheet's room detector.

:::
