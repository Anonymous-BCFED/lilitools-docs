<Breadcrumbs />

# `slavesheet slave` Subcommand

`slavesheet slave` subcommands permit bulk operations to be done on slaves.

<@cmd{"args":["slavesheet","slave","--help"]}@>

## A Note

slavesheet was made with myself and my own requirements in mind.  It may not fit your needs. While it tries to provide as much information as possible, much of it may not be applicable to you or just completely useless.  Your milage may vary.

<!-- ## Subcommands

- [add](./add.md)
- [add](./add.md)
- [add](./add.md)
- [add](./add.md)
- [add](./add.md)
- [add](./add.md) -->