# Adding Features

This is a quick guide on how to get started on adding new stuff to LiliTools.

## Before You Go Go

::: warning
Prior to submitting a Merge Request, please consider the following so we don't have to close it and cause a load of drama.

1. Is your change something that can benefit the community and not just yourself?
1. Will you be around to fix your Merge Request if we find a problem or answer questions?
1. Are you qualified enough to work on this code? Can you find someone willing to help you if you aren't?
1. Are you able to behave in a civilized manner in the comments?
:::

## Write Docs First

Writing documentation prior to beginning your coding is a good practice, as the documentation forces you to refine your idea and commit what amounts to a plan to paper.

