# Important Locations

Where to find things.

## Effects

<table>
<thead>
<th>File</th><th>Method</th><th>Notes</th>
</thead>
<tbody>
<tr>
<td rowspan="2"><strong>AbstractItemEffectType.java</strong></td>
<td><code>genericAttributeEffect</code></td>
<td>Potion effects</td>
</tr>
<tr>
<td><code>getClothingTFDescriptions</code></td>
<td>Clothing effects</td>
</tr>
</tbody>
</table>
