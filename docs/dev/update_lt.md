# After LT Updates

How to respond when LT updates:

1. Select clean, unmodded build of LT.
1. Run `python devtools/updateLT.py`
1. Run `lilirun` from `dist`
    1. Load `Female` save.
    1. Immediately save over `Female`.
    1. Close the game
1. Run `lilirun` from `dist`
    1. Load `Male` save.
    1. Immediately save over `Male`.
    1. Close the game
1. Run `python devtools/collectSaveSamples.py`
1. Update unit tests, if needed.
1. Select infodump build of LT.
1. Run `bin/lilirun -D=infodump=true -- --load Male --exit`
1. Run `python devtools/updateDataDumps.py`
1. Run `python devtools/updateLT.py`
1. Run `python devtools/cleanupGenerated.py`

## Serialization Tests
Now you must ensure (de)serialization is symmetrical.

1. Run `python devtools/build.py --skip-bins --save-comparison-mode-will-break-shit --rebuild`.
1. Run `bash devtools/TESTSAVES.sh`
1. Examine `testout.diff`.
1. Once done, rebuild normally. (`python devtools/build.py --rebuild`)

### Known False Positives
* `-/game/NPC[...]/character/body/leg/@tailLength='5.0'`
* `-/game/NPC[...]/character/characterInventory/clothingEquipped/clothing[...]/@sealed='false'`
* `-/game/NPC[...]/character/characterInventory/clothingEquipped/clothing[...]/effects/effect[...]/@timer='...'`
* `+/game/NPC[...]/character/characterInventory/clothingEquipped/clothing[...]/effects/effect[...]/@timer='...'`
* `+/game/NPC[...]/character/core/creampieRetentionAreas`
* `-/game/NPC[...]/character/slavery/slaveAssignedJobs`
* `-/game/NPC[...]/character/slavery/slaveJobSettings`