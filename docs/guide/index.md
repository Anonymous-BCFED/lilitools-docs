---
title: Introduction
next: installing
---

# What is LiliTools?

LiliTools is a set of save editors and other miscellaneous tools for the erotic text game known as Lilith's Throne ("LT").

While saves for Lilith's Throne *can* be modified relatively easily with a text editor, saves are extremely large and ponderous, often sporting tens of thousands of lines of indented "readable" XML.  The sheer size of these files often results in most text editors hanging or outright crashing.  Save XML is also very complicated.

## LiliTools Purpose

Lilitools provides a simplified command-line (and, eventually, GUI) interface to quickly and simply modify and examine the contents of LT save files.

## The Obvious

::: danger
**LiliTools can break your saves.**  Make sure you make backups before proceeding.
:::

::: warning
As a save editor, lilitools is *not* supported by the developers of Lilith's Throne or the game's modders.  By using lilitools, you can potentially break saves and trigger bugs.  **You use lilitools at your own risk**. 

So, **don't bug LT devs about bugs triggered by lilitools** in LT.  They aren't responsible, and can't fix our problems anyway.
:::

## What Comes In The Box

### lilirun

`lilirun` is a simple launcher for the game that can automatically download and install OpenJFX.

Intended for Linux users.

### lilitool

`lilitool` is the primary tool most people will be using to modify or inspect their saves.  It provides a vast and growing list of subcommands to do things like heal a character, unseal clothing, modify body parts, *et cetera*.

### slavesheet

A relatively simple tool I made to help with slave management.  Many of the features are aimed at my own needs and preferences, so <abbr title="Your Milage May Vary">YMMV</abbr>.

Features:
* A sortable, searchable table of slaves
* Job table
* Gantt chart
* Slave ID (SID) generator
  * A SID is a hash of the internal ID of the character. Example: `-1,Scarlett` &rarr; `CEAC-NH00`.  It's used like a license plate. [Technical Information](/ref/lilitools/sid)