---
title: Installing LiliTools
---

# Installing LiliTools

It is possible to install LiliTools by one of two ways.

## Binary Installation

1. Visit https://gitlab.com/Anonymous-BCFED/lilitools/-/releases
1. Obtain the latest download for your platform
1. Extract to a folder of your choice
1. Add the `bin/` directory to your OS `$PATH` so you can easily use lilitools from the command line.

## Source Install

### With `pip`

1. Install [CPython](https://python.org) >= 3.10
    * **WINDOWS USERS:** Ensure that it's set up to register stuff to PATH so you can actually use it.
1. Run the following:

```shell
# Install from our git repo
pip install -U git+https://gitlab.com/Anonymous-BCFED/lilitools.git
```