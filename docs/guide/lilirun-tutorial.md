---
title: Running lilirun
---

# Running `lilirun`

First, let's familiarize ourselves with `lilirun`.  While it's not really needed to run `lilitool`, it can be helpful for people who can't get OpenJFX running.

::: warning
`lilirun` will automatically download OpenJFX from the Internet.  If this makes you uncomfortable, don't use it.
:::

## The Basics

To just get Lilith's Throne off the ground:

1. Extract Lilith's Throne into a folder where you want to play it. This should result in a directory with the contents:
   * `LilithsThrone_#_#_#[_#].jar`
   * `res/` **(IMPORTANT)**
   * You may also have some READMEs and other stuff, if you downloaded the .zip.
1. Open a command line in this directory.
   * **Windows:**
      1. Open the Run dialog with <WinKey /> + <Key label="R" />
      1. Type in `cmd` and press <EnterKey />.
      1. After the command console opens, run `cd /d C:\Path\To\LilithsThrone\`
1. Run `lilirun`.

You should now see something like this:

<!-- This is deliberately manually-written. Copy from an actual run of the STOCK game. -->
```
LiliTools v0.0.1 - lilirun
Copyright (c)2021-2024 LiliTools Contributors.
Available under the MIT Open Source License.
Compiled against Lilith's Throne 0.4.9 (commit ed687f320265075ec5c365470265bbae046beeb4)
-------------------------------------------------
Performing idiot checks...
 - Is Java present? yes (/usr/bin/java)
 - Java version >= 17.0.8? yes (17.0.9.0)
 - Is resources directory (res) present? yes
 - Is data directory (data) present? yes
 - Is JavaFX version >= 17.0.9? (NOT FOUND)
   Could not find matching install of OpenJFX! Installing...
openjfx.zip: 100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 48.8M/48.8M [00:12<00:00, 4.10MiB/s]
Installed LT Versions:
 * 0.4.9 (/home/username/liliths_throne/LilithsThrone_0_4_9.jar)
 - 0.4.8.7 (/home/username/liliths_throne/LilithsThrone_0_4_8_7.jar)
 - 0.4.8.5 (/home/username/liliths_throne/LilithsThrone_0_4_8_5.jar)
 - 0.4.7.6 (/home/username/liliths_throne/LilithsThrone_0_4_7_6.jar)
 - 0.4.7.2 (/home/username/liliths_throne/LilithsThrone_0_4_7_2.jar)
 - 0.4.6.5 (/home/username/liliths_throne/LilithsThrone_0_4_6_5.jar)
 - 0.4.5.5 (/home/username/liliths_throne/LilithsThrone_0_4_5_5.jar)
 - 0.4.5 (/home/username/liliths_throne/LilithsThrone_0_4_5.jar)
 - 0.4.4 (/home/username/liliths_throne/LilithsThrone_0_4_4.jar)
 - 0.4.3.8 (/home/username/liliths_throne/LilithsThrone_0_4_3_8.jar)
 - 0.4.2.5 (/home/username/liliths_throne/LilithsThrone_0_4_2_5.jar)
 - 0.4.2.4 (/home/username/liliths_throne/LilithsThrone_0_4_2_4.jar)
Environment:
  __compiled__.: __nuitka_version__(major=1, minor=9, micro=6, releaselevel='release', standalone=True, onefile=True, no_asserts=True, no_docstrings=False, no_annotations=False)
  CWD..........: /home/username/liliths_throne
  FXDIR........: /home/username/liliths_throne/javafx-sdk-17
  LTJAR........: /home/username/liliths_throne/LilithsThrone_0_4_9.jar
  JAVA.........: /usr/bin/java
  JAVA_HOME....: None
  defines......: {}
$ /usr/bin/java --module-path=/home/username/liliths_throne/javafx-sdk-17/lib --add-modules=ALL-MODULE-PATH -jar /home/username/liliths_throne/LilithsThrone_0_4_9.jar
Printing to error.log
```

::: info
You may also see some additional gibberish relating to GDK errors, but that's normal.  Your game should open anyway.
:::

## Advanced

::: tip
For more information, see the [lilirun reference manual](/commands/lilirun/).
:::