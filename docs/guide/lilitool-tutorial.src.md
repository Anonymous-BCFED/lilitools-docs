---
title: lilitool Tutorial

savefile: data/saves/female.xml
---

# `lilitool` Tutorial

`lilitool` is the save editor portion of LiliTools, and provides a powerful set of command-line tools one can use to inspect and modify your saves.

## Here Be Dragons

::: danger
As a save editor, `lilitool` is _not_ supported by the developers of Lilith's Throne or the game's modders. By using `lilitool`, you can potentially break saves and trigger bugs. **You use `lilitool` at your own risk**.

So, **don't bug LT devs about bugs triggered by `lilitool`** in LT. They aren't responsible, and can't fix our problems anyway.
:::

::: info
If you're used to editing saves using a text editor, you may notice that `lilitool` "minifies" saves. This is done to reduce file size and reduce the time LT takes to load the file.
:::

## First Step

::: danger
**ALWAYS** back up your saves before messing with them.
:::

First, if you have the game open, save and exit the game.

Next, let's back up everything.

<@cmd{"args":["lilitool","backup","--data-dir","data","-m","My first backup!"],"replacements":{"\\[master \\(root\\-commit\\) ([a-f0-9]+)\\]":"[master (root-commit) 0000000]","\\[master ([a-f0-9]+)\\] My first backup!":"[master 0000000] My first backup!"}}@>

Your console should now be getting spammed as lilitool copies and cleans up files to the backup repo.

The backup repo is stored in `./backup/` and exists as a git repository so changes can be tracked.

| File extension | Meaning |
|----:|:----|
| `.raw.xml` | The actual save, without any conversion or interpretation. **Always restore from this.** |
| `.clean.xml` | The save, after having been loaded and interpreted by lilitools. |
| `.decoded.yml` | The save's actual interpreted structure as a YAML document. Somewhat simplified. |

## Common

Let's go over some common commands you might want to run.

### Heal Thyself

To heal your character:

<@cmd{"args":["lilitool","heal","data/saves/female.xml","--player","--all"]}@>

::: warning
Due to not having 100% the same code and algorithms as LT, lilitool may be a few points off. If you're unsure, check what it'll do by adding `--dry-run` to the command above.
:::

### Unseal Clothing

To unseal all articles of clothing on the player:

<@cmd{"args":["lilitool","unseal","data/saves/female.xml","--player","--all"]}@>

## More

::: info
For more information about `lilitool` subcommands, please see the [`lilitool` Command Reference Manual](/commands/lilitool/)
:::
