---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  image:
    src: /lilitool.svg
    alt: LiliTool Logo
  name: "LiliTools Documentation"
  text: "Powerful, Python-driven Save Editing And Slave Management"
  tagline: Because editing XML by hand sucks.
  actions:
    - theme: brand
      text: Get Started →
      link: /guide/
    - theme: alt
      text: API Examples
      link: /api-examples

features:
  - title: Fast
    icon: 🚀
    details: Built with Python 3 and compiled to native C++.
  - title: Simple
    icon: 🐒
    details: So simple, even an imp could do it.
  - title: Open Source
    icon: 🔓
    details: Use our code to make your own tools!
---

<style>
:root {
  --vp-home-hero-name-color: transparent;
  --vp-home-hero-name-background: -webkit-linear-gradient(120deg, #bd34fe 30%, #FF00CC);

  --vp-home-hero-image-background-image: linear-gradient(-45deg, #bd34fe 50%, #FF00CC 50%);
  --vp-home-hero-image-filter: blur(44px);
}

@media (min-width: 640px) {
  :root {
    --vp-home-hero-image-filter: blur(56px);
  }
}

@media (min-width: 960px) {
  :root {
    --vp-home-hero-image-filter: blur(68px);
  }
}
</style>