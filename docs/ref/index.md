---
sidebar: auto
---
# Reference Manual

Contains technical information about lilitools and LT itself.

## Data Structures

### LiliTools

* [SIDs](lilitools/sid)

### The Game (Lilith's Throne)

* [Character IDs](lt/character-id)