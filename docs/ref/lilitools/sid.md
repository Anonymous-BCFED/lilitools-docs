---
sidebar: auto
---

# Slave IDs (SIDs)

_Slave IDs_ (referred to colloquially as _SIDs_) are a useful tool for quickly identifying slaves.

SIDs are particularly useful for larger slave populations, when one may have a few slaves with the same first name, making it difficult to manage them at a glance. SIDs can therefore be used like a license plate or part number to ascertain which slave is which.

## How SIDs are Generated

We use the following pseudocode to generate SIDs from NPC Core IDs:

```python
def gen_sid(npc: NPC) -> str:
    # Get the character ID
    char_id: str = npc.core.id

    # Encode to utf-8 byte array
    char_id_bytes: bytes = char_id.encode('utf-8')

    # Hash to a 5 byte digest using BLAKE2B
    hash_bytes = blake2b(char_id_bytes, bytes=5)

    # Interpret as an unsigned little-endian integer
    hash_number = int(
        bytes=hash_bytes,
        endianness='little',
        unsigned=True
    )

    # Encode to Base 36
    hash_code = base36(hash_number)

    # Pad Right to 8 chars
    hash_code = hash_code.rpad(8, '0')

    # Break into 4-character chunks joined by a -
    return hash_code[0:4] + '-' + hash_code[4:8]
```

## Rationale

I had the following requirements for SIDs:

- SIDs had to be something small
- They had to be easy to remember
- They had to stay the same for each character ID
- They needed to be something that could look like a license plate or USDA Cattle Individual Number (CIN).
- They must not change due to mutations, gender changes, nor status effects.

The result is a standard, unchanging identifier that can easily be used to track slaves in a lore-friendly manner.

## Examples

| Name      | Character ID                    | SID         |
| --------- | ------------------------------- | ----------- |
| Brax      | `-1,Brax`                       | `N46N-XCSE` |
| Scarlette | `-1,Scarlett`                   | `CEAC-NH00` |
| (Random)  | `1649,DominionAlleywayAttacker` | `B1BF-UEVI` |

## Caveats

SIDs have a few minor issues, outlined below.

### Collisions

SIDs are much more prone to collisions than other options, such as [Character IDs](/ref/lt/character-id) or [UUIDs](https://en.wikipedia.org/wiki/Universally_unique_identifier). This comes down to the fact that SIDs are based upon merely 40 bits of information, when UUIDs use 128 bits and mathematically safer noise sources, and Character IDs simply use an incrementing number combined with the character type.

Collisions should not be an issue for most playthroughs, however, as they are not being used for security purposes and there are few competing IDs.

### Not Used In Game

While it would be nice if SIDs were used in vanilla LT, they aren't. This could make it difficult to pin down which slave in your list is the one paired with a given SID.

To solve this, you may decide to change a slave's name to their SID.
